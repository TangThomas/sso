package com.txh.config;

import com.txh.ssoFilter.SsoFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2018/10/22.
 */
@Configuration
public class SSoFilterConfig {

    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setName("ssoFilter");
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("paramName", "paramValue");
        filterRegistrationBean.setFilter(ssoFilter());
        return filterRegistrationBean;
    }


    @Bean
    public SsoFilter ssoFilter(){
        return new SsoFilter();
    }
}
