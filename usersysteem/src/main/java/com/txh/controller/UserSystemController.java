package com.txh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2018/10/22.
 */
@Controller
@RequestMapping("/userSystem")
public class UserSystemController {
    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}
