package com.txh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersysteemApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersysteemApplication.class, args);
	}
}
