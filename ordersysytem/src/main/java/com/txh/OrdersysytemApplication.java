package com.txh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersysytemApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersysytemApplication.class, args);
	}
}
