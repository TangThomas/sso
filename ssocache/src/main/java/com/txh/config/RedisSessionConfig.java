package com.txh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * Created by Administrator on 2018/10/17.
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds= 3600)
public class RedisSessionConfig {

}
