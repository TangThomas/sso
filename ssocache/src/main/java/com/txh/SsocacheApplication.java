package com.txh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsocacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsocacheApplication.class, args);
	}
}
