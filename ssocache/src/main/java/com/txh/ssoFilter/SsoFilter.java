package com.txh.ssoFilter;

import com.alibaba.fastjson.JSONObject;
import com.txh.utils.RestTemplateUtil;
import org.springframework.http.ResponseEntity;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/10/15.
 */
public class SsoFilter implements Filter {

    //sso认证
    private final String SSO_SERVER_URL = "http://localhost:8080/sso/auth";
    //sso token 验证
    private final String SSO_VERIFI_URL="http://localhost:8080/sso/verifi";
    //sso注销
    private final String SSO_LOGINOUT_URL="http://localhost:8080/sso/logout";
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //检查是否携带token.
        HttpServletRequest httpServletRequest =(HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String token = httpServletRequest.getParameter("token");
        Map<String,Object> info = new HashMap<>();
        //token不为空就验证
        if(token != null){
            boolean flag = verifi(servletRequest,SSO_VERIFI_URL,token);
            if (flag){
                filterChain.doFilter(servletRequest,servletResponse);
                return;
            }else{
                info.put("info",token+"无效");
                info.put("code","10010");
                httpServletResponse.getWriter().write((Json2String(info)));
                return;
            }
        }
        //如果有效并且已登录就放行到下个过滤器
        HttpSession session=httpServletRequest.getSession();
        if (session.getAttribute("flag")!=null && (boolean)session.getAttribute("flag") == true){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        //获取注销的标记销毁全局会话
        String loginOut = httpServletRequest.getParameter("logout");
        if (loginOut == "true"){
            httpServletResponse.sendRedirect(SSO_LOGINOUT_URL);
        }
        //没有token就跳转到认证中心
        //当前请求地址
        String callBackUrl = httpServletRequest.getRequestURL().toString();
        StringBuilder authUrl = new StringBuilder();
        authUrl.append(SSO_SERVER_URL).append("?callBackUrl=").append(callBackUrl);
        httpServletResponse.sendRedirect(authUrl.toString());

    }




    @Override
    public void destroy() {

    }

    private boolean verifi(ServletRequest servletRequest,String url,String token){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(url).append("?token=").append(token);
        String result = RestTemplateUtil.get(servletRequest,stringBuilder.toString(),null);
        JSONObject jsonObject=JSONObject.parseObject(result);
        if ( jsonObject.getString("code").equals("200")){
            return  true;
        }else{
            return false;
        }
    }

    private String Json2String(Object obj){
        if (obj == null){
            return null;
        }
       String jsonString= JSONObject.toJSONString(obj);
        return jsonString;
    }
}
